# MICROCONTROLLER - Arduino
## Object Detection Self Driving Car with NVIDIA Jetson TX2

>May-July. 2017
>Hackathon-HACKSJTU Shanghai 2017                                       
>Supervisor: Engineer from NVIDIA
 	
* Assembled the Self Driving Car controlled by Arduino and the NVIDIA Jetson TX2 Module.
* Installed the system and environments of OpenCV, caffe, ROS to new TX2 module.
* With Arduino, realized the ROS driving command to actual action of self-driving car.

### Other team members' contribution 

>In original HackSJTU

* some script provided by NVIDIA
* Chao Zhao dealed with the python package environment and python scripts
* Jiacheng Zhu dealed with ROS environment

>Later, the project was performed at another TX2 that Jiacheng and Wenjie brought for further developement
